import Vue from 'vue';
import Vuex from 'vuex';

// Modules
import ExampleModule from './modules/ExampleModule/ExampleModule';

// Globals
import mutations from './mutations';
import actions from './actions';
import getters from './getters';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {},
    mutations,
    actions,
    getters,    
    modules: {
        ExampleModule
    }
});
