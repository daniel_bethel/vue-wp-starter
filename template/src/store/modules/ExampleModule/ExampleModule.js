import mutations from './mutations';
import actions from './actions';
import getters from './getters';

export default {
    state: {
        title: '242Studios custom vue-cli template with webpack, vuex, vue-router, foundation, axios and global scss imports. Whaps!',
        posts: []
    },
    mutations,
    actions,
    getters
}