import axios from 'axios';
import { router } from '../routes/router';

let API_URL = process.env.API_URL || '';

export const http = axios.create({
  baseURL: API_URL
})

http.interceptors.request.use(function (config) {

  let token = localStorage.getItem('token');

  if (token) {
    config.headers['Authorization'] = `Bearer ${token}`;
  }

  return config;
}, function (error) {

  return Promise.reject(error);
})

// http.interceptors.response.use(function (response) {
      
//     return response;

//   }, function (error) {

//     if(error.response.status == 401) {
      
//       localStorage.removeItem('ship_user');
//       localStorage.removeItem('ship_token');

//       router.push( { name: 'login' } )
//     }

//     return Promise.reject(error);
// })