export default {
    getTitle: ( state ) => {
        return state.title;
    },
    getPosts: ( state ) => {
        return state.posts;
    }
}