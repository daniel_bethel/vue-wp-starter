import { http } from '../plugins/http.js'

export const testService = {
    fetchPosts() {
        return http.get('/posts');
    }
}