// Native imports
import Vue from 'vue';
import App from './App.vue';

// External imports
import { router } from './routes/router';
import { store } from './store/store';
import Vuetify from 'vuetify';
import Vuelidate from 'vuelidate'
import './stylus/main.styl'
import 'font-awesome/css/font-awesome.css'


//Use statements
Vue.use(Vuetify)
Vue.use(Vuelidate)

// Vue Instance config
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
