import { testService } from '../../../services';

export default {
    getPosts ({ commit }) {
        testService.fetchPosts()
        .then((response) => {
            commit('FETCH_POSTS', response.data)
        })
    }
}